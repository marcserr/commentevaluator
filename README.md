# JSON PAM Evaluator

This code is used to process proposal comments in JSON format to evaluate them using a proposal argument map. Thus we obtain evaluations for: the arguments, argument sets, and the proposal itself. The code also computes proposal evaluations using a naive average approach and provides the difference between both evaluation methods.

### Requirements:

Apart from basic java packages, the code requires GSON (available at: https://github.com/google/gson).

### Running the code:

Simply run JSONEvaluator.java and the code should produce a "output.txt" file with all the detailed evaluations for each proposal. The code will also print a result sum up in the console (detailing num. of proposals, num. of proposals with some comments, number of proposals with matching evaluations using both methods, etc.)  

### Results structure:

To see the results of the evaluation open the "output.txt" file. 
  
Each proposal evaluation has the same structure (notice "x" denotes a number in that space):
  
Proposal xxxx:  
posxxxx: x likes, x dislikes, eval: x  
.  
.  
.  
negxxxx: x likes, x dislikes, eval: x  
.  
.  
.  
neutxxxx: x likes, x dislikes, eval: x  
.  
.  
.  
Positive arguments evaluation: x  
Positive arguments: Likes: x Dislikes: x  
Negative arguments evaluation: x  
Negative arguments: Likes: x Dislikes: x  
Proposal xxxx evaluation: x  
Proposal xxxx evaluation with average:x  
Difference: x  
  
- Each proposal starts with its proposal id.  
  
- The following lines describe the arguments (positive, negative and neutral in that order) received by the proposal as well as detailing the number of likes and dislikes received by each argument and their "PAM" evaluation.  
  
- The next two lines inform about the "PAM" evaluation of the positive arguments, and the number of likes and dislikes received by this set.
  
- The next two lines detail this same information about negative arguments.
  
- Finally the next three lines detail the "PAM" evaluation of the proposal, the average evaluation of the proposal, and the difference between these evaluations.

Notice that in all evaluations "x" will be a number in the interval [1, 5] or NaN in the cases where the evaluation could not be computed (e.g. no likes/dislikes received).


### Sources:

This code includes:

- The norm argument map argumentation system package (available at: https://bitbucket.org/marcserr/norm-argument-map).
  
- The WOWA package (available at: http://www.mdai.cat/ifao/wowa.php?llengua=ja).
  
- The comments on the PAM participation process (available at: https://github.com/elaragon/metadecidim/tree/master/comments).