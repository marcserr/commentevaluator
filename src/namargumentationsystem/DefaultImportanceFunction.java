package namargumentationsystem;

/**
 * Implements the natural importance function as described in the project associated
 * @author Marc
 */
public class DefaultImportanceFunction extends ImportanceFunction {

	/**
	 * The spectrum of the function
	 */
	private Spectrum s;
	/**
	 * The tolerance of the function, used to control doubles
	 */
	private double tol;

	/**
	 * Creates a new DefaultImportanceFunction
	 * @param s the function's spectrum
	 * @param tol the function's tolerance
	 */
	public DefaultImportanceFunction(Spectrum s, double tol) {
		this.s = s;
		this.tol = tol;
	}

	
	public double compute(double x) {
		x = this.correct(x);
		double result;
		double ub = this.s.getUpperBound();
		double lb = this.s.getLowerBound();
		if(x <= (ub+3.*lb)/4.) {
			result = (-4.*lb*lb-1.8*lb*ub+1.*ub*ub+9.8*lb*x-0.2*ub*x-4.8*x*x)/((lb-1.*ub)*(lb-1.*ub));
		} else if(x < (3.*ub+5.*lb)/8.) {
			result = (-1.75*lb-1.45*ub+3.2*x)/(lb-1.*ub);
		} else if(x <= (5.*ub+3.*lb)/8.) {
			result = (4.*lb*lb+8.*lb*ub+4.*ub*ub-16.*lb*x-16.*ub*x+16.*x*x)/((lb-1.*ub)*(lb-1.*ub));
		} else if(x < (3.*ub+lb)/4.) {
			result = (1.45*lb+1.75*ub-3.2*x)/(lb-1.*ub);
		} else if(x <= ub){
			result = (1.*lb*lb-1.8*lb*ub-4.*ub*ub-0.2*lb*x+9.8*ub*x-4.8*x*x)/((lb-1.*ub)*(lb-1.*ub));
		} else {
			result = Double.NaN;
			System.err.println("x value out of spectrum");
		}
		return result;
	}
	
	/**
	 * Corrects x, if x is out of the spectrum for less than the tolerance of the DefaultImportanceFunction, then returns the nearest spectrum bound 
	 * @param x the double to correct
	 * @return double x corrected (or if x is out of the spectrum, even using the tolerance, returns x uncorrected)
	 */
	public double correct(double x) {
		double lb = this.s.getLowerBound();
		double ub = this.s.getUpperBound();
		if( x > ub && x < ub+this.tol){
			x = ub;
		} else if(x<lb && x>lb-this.tol)  {
			x = lb;
		}
		return x;
	}

}
