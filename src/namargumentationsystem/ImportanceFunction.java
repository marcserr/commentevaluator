package namargumentationsystem;

/**
 * Necessary layout for an importance function, as defined in the associated project
 * @author Marc
 */
public abstract class ImportanceFunction {

	/**
	 * Computes the value of the function in the point x
	 * @param x the point in which to compute the function
	 * @return double the function's value in x
	 */
	public abstract double compute(double x);
	
}
