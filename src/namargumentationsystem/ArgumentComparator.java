package namargumentationsystem;

import java.util.Comparator;

/**
 * This class implements a comparator to compare arguments using their support
 * @author Marc
 */
public class ArgumentComparator implements Comparator<Argument> {
	
	public ArgumentComparator(){};

	public int compare(Argument o1, Argument o2) {
		Double sup1 = o1.support();
		Double sup2 = o2.support();
		return sup2.compareTo(sup1);
	}

}
