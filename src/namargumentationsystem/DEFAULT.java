package namargumentationsystem;

/**
 * The default items of the norm argument map
 * @author Marc
 */
public final class DEFAULT {
	
	private DEFAULT(){}

	/**
	 * Constructs and returns the default importance function with the spectrum and tolerance given
	 * @param s the function's spectrum
	 * @param tol the function's tolerance
	 * @return ImportanceFunction the default importance function created
	 */
	public static ImportanceFunction importanceFunction(Spectrum s, double tol) {
		ImportanceFunction i = new DefaultImportanceFunction(s, tol);
		return i;
	}
	
	/**
	 * Constructs and returns the default importance function with the spectrum given and the default tolerance
	 * @param s the function's spectrum
	 * @return ImportanceFunction the default importance function created
	 */
	public static ImportanceFunction importanceFunction(Spectrum s) {
		ImportanceFunction i = new DefaultImportanceFunction(s, 1e-5);
		return i;
	}

	/**
	 * Returns the default alpha
	 * @return double the default alpha (0.3)
	 */
	public static double alpha() {
		return 0.3;
	}
}