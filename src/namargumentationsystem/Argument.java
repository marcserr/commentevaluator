package namargumentationsystem;

import java.io.*;
import java.util.*;
import ww.*;

/**
 * This class implements an argument of the norm argument map
 * @author Marc
 */
public class Argument {
	
	/**
	 * Statement of the argument
	 */
	private String statement;
	/**
	 * The opinions received by the argument
	 */
	private List<Opinion> opinions;
	/**
	 * The spectrum of possible opinion values
	 */
	private Spectrum spec;
	/**
	 * The importance function to evaluate the support
	 */
	private ImportanceFunction impfunc;
	
	/**
	 * Creates a new argument
	 * @param s the argument's statement
	 * @param spec the argument's spectrum
	 * @param i the argument's importance function
	 */
	public Argument(String s, Spectrum spec, ImportanceFunction i) {
		this.opinions = new ArrayList<>();
		this.spec = spec;
		this.setStatement(s);
		this.impfunc = i;
	}
	/**
	 * Creates a new argument
	 * @param s the argument's statement
	 * @param o the list of opinions of the argument
	 * @param spec the argument's spectrum
	 * @param i the argument's importance function
	 */
	public Argument(String s, List<Opinion> o, Spectrum spec, ImportanceFunction i) {
		this.spec = spec;
		this.setStatement(s);
		this.setOpinions(o);
		this.impfunc = i;
	}
	
	/**
	 * Creates a new opinion from the double and adds it to the argument's opinions.
	 * @param num The value of the opinion to add.
	 */
	public void addOpinion(double num) {
		if(spec.has(num)){
			Opinion o = new Opinion(num);
			this.opinions.add(o);
		} else {
			System.err.println("Opinion "+num+" out of opinion spectrum");
		}
	}
	
	/**
	 * Creates a new opinion from the double and adds it to the argument's opinions.
	 * @param o The opinion to add.
	 */
	public void addOpinion(Opinion o) {
		if(spec.has(o)){
			this.opinions.add(o);
		} else {
			System.err.println("Opinion out of opinion spectrum");
		}
	}
	
	/**
	 * Computes the support for the argument as described in the author's project
	 * @return double the support for the argument
	 */
	public double support(){
		double result;
		Vector<Double> weights = new Vector<Double>();
		Vector<Double> data = new Vector<Double>();
		double total = 0;
		if(this.opinions.isEmpty()){
			result = Double.NaN;
		} else {
			for(int i=0; i < this.opinions.size(); i++) {
				if(this.spec.has(this.opinions.get(i))){
					data.add(this.opinions.get(i).getNum());
					weights.add(this.impfunc.compute(this.opinions.get(i).getNum()));
					total += weights.get(i);
				} else{
					System.err.println("When calculating argument support: Opinion out of spectrum(it has been ignored in the argument support assessment)");
				}
			}
			for(int i=0; i<weights.size(); i++) {
				weights.set(i, weights.get(i)/total);
			}
			try {
				result = wwv2.wm(weights, data);
			} catch(Exception e) {
				System.err.println("Unable to calculate support for argument: "+this.getStatement()+"\n Weighted mean failed.");
				result = Double.NaN;
			}
		}
		return result;
	}
	
	/**
	 * Looks if the argument's opinions are defined in the spectrum passed as parameter
	 * @param spec the spectrum to compare
	 * @return Boolean true if in the spectrum false otherwise
	 */
	public Boolean in(Spectrum spec) {
		return spec.equals(this.spec);
	}
	
	/**
	 * Returns the number of opinions of the argument
	 * @return double the number of opinions
	 */
	public double numberOpinions() {
		return (double) this.opinions.size();
	}
	
	/**
	 * Returns the weight of the argument as the sum of the importance of all its opinions
	 * @return double the weight of the argument
	 */
	public double weight() {
		double number=0;
		for(int i=0; i<this.opinions.size();i++){
			number += this.impfunc.compute(this.opinions.get(i).getNum());
		}
		return number;
	}
	
	/**
	 * Prints the information of the argument, that is: statement:  support  number of opinions
	 * @param out The PrintStream in which the information is printed
	 */
	public void printStatus(PrintStream out) {
		Double support = this.support();
		if(support.isNaN()) {
			out.format("%s:  %s  %f",this.statement,"Not defined",this.numberOpinions(),this.weight());
		} else {
			out.format("%s:  %.4f  %.1f", this.statement,support,this.numberOpinions(),this.weight());
		}
	}

	public String getStatement() {
		return statement;
	}

	public void setStatement(String statement) {
		this.statement = statement;
	}

	public List<Opinion> getOpinions() {
		return opinions;
	}

	public void setOpinions(List<Opinion> opinions) {
		this.opinions.clear();
		for(int i = 0; i<opinions.size();i++) {
			this.addOpinion(opinions.get(i));
		}
	}
}
