package namargumentationsystem;

/**
 * Implements an opinion of the norm argument map
 * @author Marc
 */
public class Opinion {
	
	/**
	 * The opinion's value
	 */
	private double num;
	
	/**
	 * Creates a new opinion
	 * @param n the opinion's value
	 */
	public Opinion(double n){
		this.setNum(n);
	}

	public double getNum() {
		return num;
	}

	public void setNum(double num) {
		this.num = num;
	}
}
