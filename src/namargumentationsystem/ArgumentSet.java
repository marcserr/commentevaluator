package namargumentationsystem;

import java.io.*;
import java.util.*;
import ww.*;

/**
 * This class implements an argument set of the norm argument map
 * @author Marc
 */
public class ArgumentSet {
	
	/**
	 * The spectrum of opinions of the arguments in the set
	 */
	private Spectrum spec;
	/**
	 * The list of arguments in the set
	 */
	private List<Argument> arguments;
	/**
	 * The importance function to compute the support for the set
	 */
	private ImportanceFunction imfunc;
	
	/**
	 * Creates a new argument set
	 * @param s the set's spectrum
	 * @param i the set's importance function
	 */
	public ArgumentSet(Spectrum s, ImportanceFunction i) {
		this.arguments = new ArrayList<>();
		this.spec = s;
		this.imfunc = i;
	}
	
	/**
	 * Creates a new argument set
	 * @param a the arguments of the set in list form
	 * @param s the set's spectrum
	 * @param i the set's importance function
	 */
	public ArgumentSet(List<Argument> a, Spectrum s, ImportanceFunction i) {
		if(this.arguments == null) {
			this.arguments = new ArrayList<>();
		}
		this.spec = s;
		this.imfunc = i;
		this.setArguments(a);
	}
	
	/**
	 * Adds an argument to the set, must be in the same spectrum as the set
	 * @param a the argument to add
	 */
	public void addArgument(Argument a){
		if(a.in(this.spec)){
			this.arguments.add(a);
		}
	}
	
	/**
	 * Creates a new argument and adds it to the set
	 * @param s the argument's statement
	 * @param o the argument's list of opinions
	 */
	public void addArgument(String s, List<Opinion> o){
		Argument a = new Argument(s,this.spec, this.imfunc);
		for(int i = 0; i< o.size();i++) {
			a.addOpinion(o.get(i));
		}
		this.addArgument(a);
	}
	
	/**
	 * Returns the alpha-relevant arguments of the set as a new argument set
	 * @param num the minimum number of opinions to be considered alpha-relevant
	 * @return ArgumentSet the alpha-relevant argument set
	 */
	public ArgumentSet alphaRelevantArguments(double num) {
		List<Argument> relevantlist = new ArrayList<>();
		for(int i = 0; i< this.arguments.size();i++) {
			Argument arg = this.arguments.get(i);
			Boolean passedMiddleSpec = (arg.support() > this.spec.middle());
			Boolean passedMinNumOpinions = (arg.numberOpinions() >= num);
			if(passedMiddleSpec && passedMinNumOpinions) {
				relevantlist.add(arg);
			}
			
		}
		ArgumentSet relevant = new ArgumentSet(relevantlist, this.spec, this.imfunc);
		return relevant;
	}
	
	/**
	 * Returns the number of opinions of the set
	 * @return double the number of opinions of the set
	 */
	public double numberOpinions() {
		double num = 0;
		List<Argument> set = new ArrayList<>();
		set = this.arguments;
		for(int i = 0; i<set.size(); i++) {
			num += set.get(i).numberOpinions();
		}
		return num;
	}
	/**
	 * Returns the weight of the argument set as the sum of the importance of all its alpha-relevant arguments
	 * @param minimum the minimum opinions to be considered alpha-relevant
	 * @return the weight of the argument set
	 */
	public double weight(double minimum){
		ArgumentSet relevant = this.alphaRelevantArguments(minimum);
		return relevant.weight();
	}
	
	/**
	 * Returns the weight of the argument set as the sum of the importance of all its arguments
	 * @return the weight of the argument set
	 */
	public double weight() {
		double number=0;
		for(int i=0; i<this.arguments.size();i++){
			number += this.arguments.get(i).weight();
		}
		return (double) number;
	}
	
	/**
	 * Returns the number of alpha-relevant arguments' opinions
	 * @param numrel the minimum number of opinions to be considered alpha-relevant
	 * @return double the number of alpha-relevant arguments' opinions
	 */
	public double numberRelevantOpinions(double numrel) {
		double num = 0;
		List<Argument> set = new ArrayList<>();
		set = this.alphaRelevantArguments(numrel).getArguments();
		for(int i = 0; i<set.size(); i++) {
			num += set.get(i).numberOpinions();
		}
		return num;
	}

	/**
	 * Assesses the support for the set
	 * @param num the minimum number of opinions to be considered alpha-relevant argument
	 * @return double the support for the set
	 */
	public double support(double num){
		Vector<Double> wmweights = new Vector<Double>();
		Vector<Double> owaweights = new Vector<Double>();
		Vector<Double> data = new Vector<Double>();
		ArgumentSet relevant = this.alphaRelevantArguments(num);
		List<Argument> relevantlist = relevant.getArguments();
		Collections.sort(relevantlist, new ArgumentComparator());
		double result;
		if (relevantlist.isEmpty()){
			result = Double.NaN;
		} else {
			double totalwm = 0;
			double totalowa = 0;
			for(int i = 0; i < relevantlist.size();i++) {
				data.add(relevantlist.get(i).support());
				wmweights.add(relevantlist.get(i).weight());
				totalwm += wmweights.get(i);
				owaweights.add(this.imfunc.compute(relevantlist.get(i).support()));
				totalowa += owaweights.get(i);
			}
			for(int i=0; i<owaweights.size();i++) {
				owaweights.set(i, owaweights.get(i)/totalowa);
				wmweights.set(i, wmweights.get(i)/totalwm);
			}
			try{
				result = wwv2.wowa(owaweights, wmweights, data);
			}catch(Exception e){
				System.err.println("Unable to calculate support for argument set.\n WOWA failed.");
				result = Double.NaN;
			}
		}
		return result;
	}
	
	/**
	 * Prints the information of the set, that is the information of each argument and the support for the set
	 * @param out the PrintStream to print the information
	 * @param num the minimum number of opinions to be considered alpha-relevant
	 */
	public void printStatus(PrintStream out, double num) {
		List<Argument> relevantArgs = this.alphaRelevantArguments(num).getArguments();
		for(int i = 0; i<this.arguments.size(); i++){
			this.arguments.get(i).printStatus(out);
			if(relevantArgs.contains(this.arguments.get(i))) {
				System.out.format("  %s","alpha-relevant");
			} else {
				System.out.format("  %s","not alpha-relevant");
			}
			System.out.println("");
		}
	}
	
	public List<Argument> getArguments() {
		return arguments;
	}

	public void setArguments(List<Argument> arguments) {
		this.arguments.clear();
		for(int i = 0; i<arguments.size();i++) {
			this.addArgument(arguments.get(i));
		}
	}

	public ImportanceFunction getImportanceFunction() {
		return imfunc;
	}
	
}
