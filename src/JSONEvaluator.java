
import java.io.*;
import java.util.*;
import com.google.gson.*;
import namargumentationsystem.*;

/**
 * This class implements a nam builder for json files representing comments and evaluates it
 * @author Marc
 */
public class JSONEvaluator {
	
	/**
	 * Parameter to consider two evaluations largely different (eg. if |eval_average - eval_nam| >= EPSILON, the evaluations are largely different) 
	 */
	private Double EPSILON = 1.;
	
	/**
	 * List of comment JSON files to evaluate 
	 */
	private List<String> files;
	
	/**
	 * Output of the evaluation
	 */
	private PrintWriter output;
	
	/**
	 * Different structures to process the actual evaluation
	 */
	
	private JsonParser parser;
	private NormArgumentMap nam;
	private Spectrum spec;
	private DefaultImportanceFunction impfunc;
	private List<Argument> neutralcomments;
	
	/**
	 * The following are variables to count the occurrences expressed in their name
	 */
	
	private Integer proposals = 0;
	private Integer anyFeedback = 0;
	private Integer noFeedback = 0;
	private Integer noComments = 0;
	private Integer differentOutcome = 0;
	private Integer insuficientFeedback = 0;
	private Integer bigDifferenceEval = 0;
	private Integer avgmatch = 0;
	private Integer avgexpandmatch = 0;
	private Integer addmatches = 0;
	private Integer addmatchesexp = 0;
	private Integer neut_only = 0;
	private Integer someFeedback = 0;
	
	public JSONEvaluator() {
		this.parser = new JsonParser();
		this.spec = new Spectrum(1., 5.);
		this.impfunc = new DefaultImportanceFunction(this.spec, 0.0001);
		this.files = new ArrayList<String>();
		this.neutralcomments = new ArrayList<Argument>();
	}
	
	public void setOutput(String outFileName) {
		try {
			this.output = new PrintWriter(outFileName, "UTF-8");
		} catch(Exception e) {
		}
	}
	
	/**
	 * Given a directory address, the program evaluates the files in it
	 * @param inDir The directory to evaluate
	 * @throws IOException The directory inDir does not exist
	 */
	public void evalDir(String inDir) throws IOException {
		File folder = new File(inDir);
		File[] folderFiles = folder.listFiles(); 
		Arrays.sort(folderFiles);
		for (File file : folderFiles) {
		    if (file.isFile()) {
		    	String name = file.getCanonicalPath().toString();
		        this.files.add(name);
		    }
		}
		for(String filename : this.files) {
			this.readAndEval(filename, inDir);
		}
	}
	
	/**
	 * Reads the JSON files of a single proposal
	 * @param s The first filename of the proposal
	 * @param inDir The JSON files directory
	 */
	public void readAndEval(String s, String inDir) {
		if(s.contains("-01.json")) {
			this.proposals += 1;
			try {
				List<JsonObject> jsons = new ArrayList<JsonObject>();
				int num = 1;
				String root = s.replace("-01.json", "");
				String filename = s;
				File f = new File(s);
				while(f.exists()){
					Object obj = this.parser.parse(new FileReader(s));
					JsonObject json = (JsonObject) obj;
					jsons.add(json);
					num += 1;
					String snum;
					if(num < 10) {
						snum = "0"+String.valueOf(num);
					} else {
						snum = String.valueOf(num);
					}
					filename = root+"-"+snum+".json";
					f = new File(filename);
				}
				this.nam = new NormArgumentMap(new Norm("Proposal "+s.replace("-01.json", "").replace(inDir, "")), this.spec);
				this.evaluateComments(jsons);
			} catch(FileNotFoundException e) {
				this.output.println("File not found");
			}
			
		}
	}
	
	/**
	 * Given a list of JSON objectes, it evaluates the comments in it
	 * @param jsons A list of JSON objects
	 */
	public void evaluateComments(List<JsonObject> jsons) {
		for(JsonObject json : jsons) {
			JsonArray comments = json.get("comments").getAsJsonArray();
			this.buildNAM(comments);
		}
		this.evaluateNAM();
		
	}
	
	/**
	 * Builds a norm argument map structure from an array of comments, that is categorizing them into positive/negative comments and taking into account their votes  
	 * @param comments Array of comments from which the norm argument map will be built
	 */
	public void buildNAM(JsonArray comments) {
		List<String> posargsids = new ArrayList<>();
		List<String> negargsids = new ArrayList<>();
		List<String> neutargsids = new ArrayList<>();
		this.neutralcomments = new ArrayList<Argument>();
		Iterator<JsonElement> itr = comments.iterator(); 
		while (itr.hasNext())  {
			JsonObject comment = (JsonObject) itr.next();
			Argument arg = new Argument(comment.get("id").getAsString(), this.spec, this.impfunc);
			for(int i = 0; i< comment.get("total_likes").getAsInt(); i++){
				arg.addOpinion(5.);
			}
			for(int i = 0; i< comment.get("total_dislikes").getAsInt(); i++){
				arg.addOpinion(1.);
			}
			if(!comment.get("alignment").isJsonNull() && comment.get("alignment").getAsInt() == 1) {
				this.nam.addArgument(arg, true);
				posargsids.add(comment.get("id").getAsString());
			} else if (!comment.get("alignment").isJsonNull() && comment.get("alignment").getAsInt() == -1) {
				this.nam.addArgument(arg, false);
				negargsids.add(comment.get("id").getAsString());
			}
			else if (comment.get("alignement") == null && !comment.get("ancestry").isJsonNull()){
				String[] treePath = comment.get("ancestry").getAsString().split("/");
				String root = treePath[0];
				Integer al = null;
				for (String pid: posargsids) {
					if(pid.equals(root)) {
						al = 1;
					}
				}
				if(al == null) {
					for(String nid: negargsids){
						if(nid.equals(root)) {
							al = -1;
						}
					}
				}
				if(al == null) {
					for(String nid: neutargsids){
						if(nid.equals(root)) {
							al = 0;
						}
					}
				}
				Integer finalal = al;
				if(treePath.length%2 == 1 && al != 0) {
					finalal = finalal * -1;
				}
				if(finalal.equals(1)) {
					this.nam.addArgument(arg, true);
				} else if(finalal.equals(-1)){
					this.nam.addArgument(arg, false);
				} else if(finalal.equals(0)){
					this.neutralcomments.add(arg);
				} else {
					System.out.println("ERROR");
				}
			} else {
				this.neutralcomments.add(arg);
				neutargsids.add(comment.get("id").getAsString());
			}
		}
	}
	
	/**
	 * Evaluates the NAM that has been built in the buildNAM method, therefore giving scores to comments, comment sets, and proposal
	 */
	public void evaluateNAM() {
		Integer total_pos_likes = 0;
		Integer total_pos_dislikes = 0;
		Integer total_neg_likes = 0;
		Integer total_neg_dislikes = 0;
		Integer total_neut_likes = 0;
		Integer total_neut_dislikes = 0;
		Integer pos_args = 0;
		Integer neg_args = 0;
		Integer neut_args = 0;
		this.output.println(this.nam.getNorm()+": ");
		for(Argument arg : this.nam.getPositiveArgs().getArguments()) {
			Integer dis = 0;
			Integer like = 0;
			pos_args += 1;
			for(Opinion op : arg.getOpinions()) {
				if(op.getNum()-1.0001 < 0) {
					dis +=1;
				} else {
					like += 1;
				}
			}
			total_pos_likes += like;
			total_pos_dislikes += dis;
			this.output.println("pos"+arg.getStatement()+": "+like+" likes, "+dis+" dislikes, eval: "+arg.support());
		}
		for(Argument arg : this.nam.getNegativeArgs().getArguments()) {
			Integer dis = 0;
			Integer like = 0;
			neg_args += 1;
			for(Opinion op : arg.getOpinions()) {
				if(op.getNum()-1.0001 < 0) {
					dis +=1;
				} else {
					like += 1;
				}
			}
			total_neg_likes += like;
			total_neg_dislikes += dis;
			double argsupport = arg.support();
			this.output.println("neg"+arg.getStatement()+": "+like+" likes, "+dis+" dislikes, eval: "+argsupport);
		}
		for(Argument arg : this.neutralcomments) {
			Integer dis = 0;
			Integer like = 0;
			neut_args += 1;
			for(Opinion op : arg.getOpinions()) {
				if(op.getNum()-1.0001 < 0) {
					dis +=1;
				} else {
					like += 1;
				}
			}
			total_neut_likes += like;
			total_neut_dislikes += dis;
			this.output.println("neut"+arg.getStatement()+": "+like+" likes, "+dis+" dislikes, eval: "+arg.support());
			if(total_pos_likes+total_pos_dislikes+total_neg_likes+total_neg_dislikes == 0){
				Integer neut_avg = 3;
				if(total_neut_likes + total_neut_dislikes > 0) {
					this.neut_only += 1;
					neut_avg = (total_neut_likes*5+total_neut_dislikes*1)/(total_neut_likes+total_neut_dislikes);
					if(neut_avg > 2. && neut_avg < 4.) {
						this.addmatches += 1;
					}
					if(neut_avg > 1.5 && neut_avg < 4.5) {
						this.addmatchesexp += 1;
					}
				}					
				
			}
		}
		if(pos_args + neg_args + neut_args ==0) {
			this.noComments += 1;
		}
		Double average = ((total_pos_likes+total_neg_dislikes)*5.+(total_pos_dislikes+total_neg_likes)*1.)/(total_pos_likes+total_pos_dislikes+total_neg_likes+total_neg_dislikes);
		Double support = (Double) this.nam.support(0.3);
		if((average < 3. && support >= 3.) || (support<3 && average >= 3)) {
			this.differentOutcome +=1;
		}
		if(average<= 2 && support <= 2) {
			this.avgmatch += 1;
		} else if(average <= 2.5 && support <= 2.5){
			this.avgexpandmatch += 1;
		} else {
			if(average >= 4 && support >= 4) {
				this.avgmatch += 1;
			} else if(average >=3.5 && support >= 3.5) {
				this.avgexpandmatch += 1;
			} else {
				if(average>2 && average <4 && support >2 && support <4) {
					this.avgmatch += 1;
				} else if(average>1.5 && average <4.5 && support >1.5 && support <4.5) {
					this.avgexpandmatch += 1;
				}
			}
		}
		this.output.println("Positive arguments evaluation: "+this.nam.getPositiveArgs().support(this.nam.minimumOpinions(0.3)));
		this.output.println("Positive arguments: Likes: "+total_pos_likes+" Dislikes: "+total_pos_dislikes);
		this.output.println("Negative arguments evaluation: "+this.nam.getNegativeArgs().support(this.nam.minimumOpinions(0.3)));
		this.output.println("Negative arguments: Likes: "+total_neg_likes+" Dislikes: "+total_neg_dislikes);
		this.output.println(this.nam.getNorm()+" evaluation: "+support);
		this.output.println(this.nam.getNorm()+" evaluation with average:"+average);
		if(average != 0 && !support.isNaN()) {
			Double difference = average-support;
			this.output.println("Difference: "+difference);
			if(Math.abs(difference) >= EPSILON) {
				this.bigDifferenceEval += 1;
			}
		} else if(support.isNaN() && total_pos_likes+total_pos_dislikes+total_neg_likes+total_neg_dislikes+total_neut_likes+total_neut_dislikes >0) {
			this.insuficientFeedback += 1;
		} else if(total_pos_likes+total_pos_dislikes+total_neg_likes+total_neg_dislikes+total_neut_likes+total_neut_dislikes == 0) {
			this.noFeedback += 1;
		} 
		
		if(total_pos_likes+total_pos_dislikes+total_neg_likes+total_neg_dislikes+total_neut_likes+total_neut_dislikes > 0) {
			this.someFeedback += 1;
		}
		
		this.output.flush();
	}
	
	public void closeOutput() {
		this.output.flush();
		this.output.close();
		
	}
	
	/**
	 * Method to print all the information regarding the number of occurrences of certain 
	 */
	public void showInfo() {
		this.anyFeedback = this.proposals-this.noFeedback;
		System.out.println("Number of proposals: "+String.valueOf(this.proposals));
		System.out.println("Number of proposals with no comments: "+String.valueOf(this.noComments));
		System.out.println("Number of proposals with some comments: "+String.valueOf(this.proposals- this.noComments));
		System.out.println("Number of proposals with any feedback: "+String.valueOf(this.anyFeedback));
		System.out.println("Number of proposals with feedback not being evaluated: "+String.valueOf(this.insuficientFeedback));
		System.out.println("Number of proposals (excluding not evaluated ones) with big difference in their evaluation (compared to the mean): "+String.valueOf(this.bigDifferenceEval));
		System.out.println("Number of matches with AVG: "+String.valueOf(this.avgmatch));
		System.out.println("Number of matches with AVG (expanded interval): "+String.valueOf(this.avgmatch+ this.avgexpandmatch));
		System.out.println("Number of proposals with participation only in neutral arguments: "+String.valueOf(this.neut_only));
		System.out.println("Number of matches with AVG considering only proposals with participation only in neutral arguments: "+String.valueOf(this.addmatches));
		System.out.println("Number of matches with AVG (expanded interval) considering only proposals with participation only in neutral arguments: "+String.valueOf(this.addmatchesexp));
	}
	
	public static void main(String[] args) throws IOException  {
		JSONEvaluator jsoneval = new JSONEvaluator();
		jsoneval.setOutput("output.txt");
		jsoneval.evalDir("/Users/Marc/eclipse-workspace/CommentEvaluator/comments/");
		jsoneval.closeOutput();
		jsoneval.showInfo();
    }
}