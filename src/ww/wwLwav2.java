package ww;

//import ww.wwLp;
//import ww.wwLdf; 
import java.util.Vector;
import java.util.Enumeration;

public class wwLwav2 {
  double uniti[];
  int  lon; 
  static boolean DEBON = false; 

  wwLwav2 () {
    lon = 10;
    uniti = new double [10+1];
  };

  wwLwav2 (int LARRAY) {
    lon = LARRAY;
    uniti = new double [LARRAY+1];
  };

  public wwLwav2 (Vector v) {
    lon = v.size();
    uniti = new double [lon + 1];
    Enumeration p = v.elements();
    for (int i=1; p.hasMoreElements(); i++) { 
      uniti[i] = ((Double)p.nextElement()).doubleValue();
    }
  }
    
  void escriu () { 
    for (int i=1; i<=lon; i++) { System.out.print (uniti[i]+" "); }
    System.out.print("\n"); }

  public wwLdf setQ (int num_values) throws Exception {
    // de vLp se'n necessita un punt de mes perque es va a llo+1
    wwLp vLp = new wwLp (num_values+1); 
    wwLdf df = new wwLdf (num_values+1); /* es lo que se devuelve */
    int i;
    double tempx, llo;

    if (DEBON) System.out.println("\nsetQ");
    llo = num_values;
    vLp.punti[1].x = 0.0;
    vLp.punti[1].y = 0.0;
    for (i=2; i<=llo+1; i++)
    {
       tempx = i - 1;
       vLp.punti[i].x = tempx / llo;
       vLp.punti[i].y = uniti[i-1] + vLp.punti[i-1].y;
       if (DEBON) {
	 System.out.println("\nprimera parte de setQ");
	 System.out.println("\nvLp.punti["+i+"].x="+vLp.punti[i].x);
	 System.out.println("\nvLp.punti["+i+"].y="+vLp.punti[i].y);
       }
    }
    // df.initLdf(num_values);
    df.ferQ(vLp, num_values);
    return (df);
  }  /* esetQ */

  /* AQUESTA FUNCIO EN AQUEST MOMENT NO LA CRIDA NINGU */
  /* NO USA PER A RES EL TIPUS wwLwa QUE EL CRIDA      */
  public wwLdf setQ2 (int num_values) throws Exception 
  {
    wwLp vLp = new wwLp (num_values);
    wwLdf df = new wwLdf (num_values); /* es lo que se devuelve */
    int i;
    double tempx, llo;

    if (DEBON) System.out.println("setQ\n");

    llo = num_values;
    vLp.punti[1].x = 0.0;
    vLp.punti[1].y = 0.0;
    System.out.println("\n");
    for (i=2; i<=llo+1; i++)
    {
       tempx = i - 1;
       vLp.punti[i].x = tempx / llo;
       if (vLp.punti[i].x < 0.5)
       {
      vLp.punti[i].y = (vLp.punti[i].x / 0.5) * (vLp.punti[i].x / 0.5) * 0.5;
       }
       else
       {
	 vLp.punti[i].y = 1.0 - ((1.0 - vLp.punti[i].x) / 0.5) *
				((1.0 - vLp.punti[i].x) / 0.5)
				* 0.5 ;
       }
       System.out.println("vLp.punti["+i+"].x="+vLp.punti[i].x);
       System.out.println("vLp.punti["+i+"].y="+vLp.punti[i].y);
    }
    df.ferQ(vLp, num_values);
    return (df);
  }  /* esetQ2 */

}




